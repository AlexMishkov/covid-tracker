<?php
require_once 'DB.php';
use Create\DB;
    
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CDN -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"
        integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

    <!-- Personal CSS -->
    <link rel="stylesheet" href="assets/myCss/style.css">

    <title>Covid-tracker</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand " href="index.php"><img src="assets/images/covidlogo.png" class="ml-5"
                style="height: 5vh" alt="">Covid-Tracker</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto mr-5">
                <li class="nav-item active">
                    <a class="nav-link mr-4" href="index.php">Home <span class="sr-only">(current)</span></a>
                </li>

        </div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 px-0">
                <div id="carouselExampleCaptions" class="carousel slide " data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="assets/images/covid-19.jpg" class="d-block w-100" style="height: 70vh" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="assets/images/covid-19-2.jpg" class="d-block w-100" style="height: 70vh"
                                alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="assets/images/covid-19-3.jpg" class="d-block w-100" style="height: 70vh"
                                alt="...">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

                    <div class="col-4 offset-4 mt-5">
                        <p class="font-weight-bold text-center" style="font-size: 30px">Choose a country</p>
                        <select name="" id="countries" class="form-control">
                            <option value="global">Global</option>
                            <?php 
                                DB::getCountries();
                            ?>
                        </select>
                    </div>
            </div>
            <div class="col-12 d-flex mt-5">
                <a href="index.php" class="btn btn-primary mr-2">Today</a>
                <a href="index.php?global=lastMonth" class="btn btn-primary mr-2">Last
                    month</a>
                <a href="index.php?global=last3Months" class="btn btn-primary mr-2">Last
                    three months</a>
                    <div class="d-flex  ml-auto">
                    <!-- In production phase synching will be done with CRON job -->
                    <a href="syncCases.php" class="btn btn-info">Sync cases</a>
                    </div>
                

            </div>
        </div>
        <?php if($_GET['global'] == 'last3Months'){?>
        <div class="row d-flex justify-content-around mb-5">
            <?php DB::globalSumsLastThreeMonths() ?>
        </div>
        <?php }elseif($_GET['global'] == 'lastMonth'){ ?>
        <div class="row d-flex justify-content-around mb-5">
            <?php DB::globalSumsLastMonth() ?>
        </div>
        <?php }else{ ?>
        <div class="row d-flex justify-content-around mb-5">
            <?php DB::globalSumsToday() ?>
        </div><?php } ?>
        <div class="row mt-5">
            <div class="col-6 offset-3">
                <canvas id="myChart"></canvas>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-12 px-0">
                <footer class="text-center footer-bg">
                    <h6 class="py-4 mb-0">Created with &#10084; for Brainster</h6>
                </footer>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js"
        integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script src="assets/js/script.js"></script>

</body>

</html>