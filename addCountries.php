<?php
require_once 'DB.php';
use Create\DB;

$countries = file_get_contents('https://api.covid19api.com/countries');

$countries = json_decode($countries, true);


if(DB::checkCountries() == false){
    DB::insertCountries($countries);
}
