$(document).ready(function() {
    let countries = $('#countries');
    $(countries).on('change', function(event){
        // event.preventDefault();
        window.location.replace("countryCases.php?country=" + countries.val());
        countries.val() = getUrlParameter('country');
    });


    let date = $('#date').html();
    let active = $('#active-cases').html();
    let confirmed = $('#confirmed-cases').html();
    let deaths = $('#deaths').html();
    let recovered = $('#recovered').html();
    let myChart = document.getElementById('myChart').getContext('2d');
    let chart = new Chart(myChart, {
        type: 'bar',
        data: {
            labels: [date],
            datasets: [
            {
                label: "Confirmed",
                data: [confirmed],
                backgroundColor: "tomato",
            },
            {
                label: "Deaths",
                data: [deaths],
                backgroundColor: "lightgray"
            },
            {
                label: "Recovered",
                data: [recovered],
                backgroundColor: "palegreen"
            }
            ]     
        },
        options: {
          scales: {
            xAxes: [{
              display: true,
              gridLines: {
                display: true,
                color: "rgba(0, 0, 0, 0.2)"
              },
            }],
            yAxes: [{
              display: true,
              gridLines: {
                display: true,
                color: "rgba(0, 0, 0, 0.2)"
              },
            }]
          }
        }
    });
})