<?php
namespace Create;
require_once "constants.php";
use \PDO as PDO;

class DB {
    private static $pdo = null;

    public static function connect(){
        if(is_null(self::$pdo)){
            try{
                self::$pdo = new PDO("mysql:dbname=" . DBNAME . ";host=" . HOST, MYACCOUNT, MYPASSWORD);
            }catch(PDOException $e){
                die("Can't connect");
            }
            
        }
    }

    public static function insertCountries($countries){
        self::connect();
        $sql = "INSERT INTO countries(name, slug) VALUES(:name, :slug)";
        $stmt = self::$pdo->prepare($sql);
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":slug", $slug);
        foreach($countries as $country){
            $name = $country["Country"];
            $slug = $country["Slug"];
            $stmt->execute();
        }
    }

    public function checkCountries(){
        self::connect();
        $sql = "SELECT * FROM countries";
        $stmt = self::$pdo->query($sql);
        $i = 0;
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $i++;
        }
        if($i > 0){
            return true;
        }else{
            return false;
        }
    }

    public function getCountries(){
        self::connect();
        $sql = "SELECT * FROM countries";
        $stmt = self::$pdo->query($sql);
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $sql2 = "SELECT * FROM `cases` WHERE country_id = (SELECT id from countries WHERE slug = '$row[slug]')";
            $stmt2 = self::$pdo->query($sql2);
            $row2 = $stmt2->fetch(PDO::FETCH_ASSOC);
            // var_dump($row2);
            // die();
            if(!$row2){
                continue;
            }
            echo "<option value='$row[slug]'>$row[name]</option>";
            
        }
    }


    public static function syncCases(){
        self::connect();
        $sql = "SELECT * FROM countries";
        $stmt = self::$pdo->query($sql); 
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $slug = $row['slug'];             
                $cases = file_get_contents("https://api.covid19api.com/total/country/$slug");

                $cases = json_decode($cases, true);
                $sql2 = "INSERT INTO cases(country_id, active, recovered, deaths, confirmed, date)
                            VALUES(:country_id, :active, :recovered, :deaths, :confirmed, :date)";
                $stmt2 = self::$pdo->prepare($sql2);
                $countryId = $row['id'];
                $stmt2->bindParam(":country_id", $countryId);
                $stmt2->bindParam(":active", $active);
                $stmt2->bindParam(":recovered", $recovered);
                $stmt2->bindParam(":deaths", $deaths);
                $stmt2->bindParam(":confirmed", $confirmed);
                $stmt2->bindParam(":date", $date);
                
                
                foreach($cases as $case){
                    if(!is_array($case)){
                        continue;
                    }
                    $active = $case['Active'];
                    $recovered = $case['Recovered'];
                    $deaths = $case['Deaths'];
                    $confirmed = $case['Confirmed'];
                    $date = $case['Date'];
                    $date = date("Y-m-d", strtotime($date));
                    $sql3 = "SELECT * FROM `cases` WHERE country_id = (SELECT id from countries WHERE slug = '$slug') AND active = $active AND recovered = $recovered 
                            AND deaths = $deaths AND confirmed = $confirmed AND `date` = '$date'";
                    $stmt3 = self::$pdo->query($sql3);
                    
                        $row2 = $stmt3->fetch(PDO::FETCH_ASSOC);
                        if($row2){
                            continue;
                    } 
                    $stmt2->execute();

                }
                
        }

        return header('Location: index.php');
    }

    public function globalSumsToday(){
        self::connect();

        $date = date("Y-m-d", strtotime('-1 day'));
        $yesterday = date("Y-m-d", strtotime('-2 day'));
        $sql = "SELECT * FROM `cases` WHERE date = '$date' GROUP BY country_id";
        $stmt = self::$pdo->query($sql);
        $activeToday = 0;
        $recoveredToday = 0;
        $deathsToday = 0;
        $confirmedToday = 0;
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $activeToday += $row['active'];
            $recoveredToday += $row['recovered'];
            $deathsToday += $row['deaths'];
            $confirmedToday += $row['confirmed'];
        };

        $sql2 = "SELECT * FROM `cases` WHERE date = '$yesterday' GROUP BY country_id";
        $stmt2 = self::$pdo->query($sql2);
        $activeYesterday = 0;
        $recoveredYesterday = 0;
        $deathsYesterday = 0;
        $confirmedYesterday = 0;
        while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)){
            $activeYesterday += $row2['active'];
            $recoveredYesterday += $row2['recovered'];
            $deathsYesterday += $row2['deaths'];
            $confirmedYesterday += $row2['confirmed'];
        };

        echo "<div class='col-2 py-5 d-flex rounded my-border-primary mt-4 flex-column div-primary'>";
        echo "<h1 class='text-primary' id='active-cases'>" . $activeToday . "</h1>";
        echo "<h4 class='text-primary'>Total active cases</h4>";
        echo "<h4 class='d-none' id='date'>$date</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border-success mt-4 flex-column div-success'>";
        echo "<h1 class='text-success' id='recovered'>" . ($recoveredToday-$recoveredYesterday) . "</h1>";
        echo "<h4 class='text-success'>Total recovered today</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border-secondary mt-4 flex-column div-secondary'>";
        echo "<h1 class='text-secondary' id='deaths'>" . ($deathsToday - $deathsYesterday) . "</h1>";
        echo "<h4 class='text-secondary'>Total deaths today</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border-danger mt-4 flex-column div-danger'>";
        echo "<h1 class='text-danger' id='confirmed-cases'>" . ($confirmedToday-$confirmedYesterday) . "</h1>";
        echo "<h4 class='text-danger'>Total confirmed cases today</h4>";
        echo "</div>";

    }

    public function globalSumsLastMonth(){
        self::connect();

        $date = date("Y-m-d", strtotime('-1 day'));
        $lastMonth = date("Y-m-d", strtotime('-1 day -1 month'));
        $sql = "SELECT * FROM `cases` WHERE date = '$date' GROUP BY country_id";
        $stmt = self::$pdo->query($sql);
        $activeToday = 0;
        $recoveredToday = 0;
        $deathsToday = 0;
        $confirmedToday = 0;
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $activeToday += $row['active'];
            $recoveredToday += $row['recovered'];
            $deathsToday += $row['deaths'];
            $confirmedToday += $row['confirmed'];
        };

        $sql2 = "SELECT * FROM `cases` WHERE date = '$lastMonth' GROUP BY country_id";
        $stmt2 = self::$pdo->query($sql2);
        $activeLastMonth = 0;
        $recoveredLastMonth = 0;
        $deathsLastMonth = 0;
        $confirmedLastMonth = 0;
        while($row2 = $stmt2->fetch(PDO::FETCH_ASSOC)){
            $activeLastMonth += $row2['active'];
            $recoveredLastMonth += $row2['recovered'];
            $deathsLastMonth += $row2['deaths'];
            $confirmedLastMonth += $row2['confirmed'];
        };

        echo "<div class='col-2 py-5 d-flex rounded my-border-primary mt-4 flex-column div-primary'>";
        echo "<h1 class='text-primary' id='active-cases'>" . $activeToday . "</h1>";
        echo "<h4 class='text-primary'>Total active cases</h4>";
        echo "<h4 class='d-none' id='date'>$lastMonth - $date</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border-success mt-4 flex-column div-success'>";
        echo "<h1 class='text-success' id='recovered'>" . ($recoveredToday-$recoveredLastMonth) . "</h1>";
        echo "<h4 class='text-success'>Total recovered in the last month</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border-secondary mt-4 flex-column div-secondary'>";
        echo "<h1 class='text-secondary' id='deaths'>" . ($deathsToday-$deathsLastMonth) . "</h1>";
        echo "<h4 class='text-secondary'>Total deaths in the last month</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border-danger mt-4 flex-column div-danger'>";
        echo "<h1 class='text-danger' id='confirmed-cases'>" . ($confirmedToday-$confirmedLastMonth) . "</h1>";
        echo "<h4 class='text-danger'>Total confirmed cases in the last month</h4>";
        echo "</div>";

    }

    public function globalSumsLastThreeMonths(){
        self::connect();

        $date = date("Y-m-d", strtotime('-1 day'));
        $threeMonthsAgo = date("Y-m-d", strtotime('-1 day -3 month'));
        $sql = "SELECT * FROM `cases` WHERE date = '$date' GROUP BY country_id";
        $stmt = self::$pdo->query($sql);
        $activeToday = 0;
        $recoveredToday = 0;
        $deathsToday = 0;
        $confirmedToday = 0;
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $activeToday += $row['active'];
            $recoveredToday += $row['recovered'];
            $deathsToday += $row['deaths'];
            $confirmedToday += $row['confirmed'];
        };

        $sql = "SELECT * FROM `cases` WHERE date = '$threeMonthsAgo' GROUP BY country_id";
        $stmt = self::$pdo->query($sql);
        $activeLast3Month = 0;
        $recoveredLast3Month = 0;
        $deathsLast3Month = 0;
        $confirmedLast3Month = 0;
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $activeLast3Month += $row['active'];
            $recoveredLast3Month += $row['recovered'];
            $deathsLast3Month += $row['deaths'];
            $confirmedLast3Month += $row['confirmed'];
        };

        echo "<div class='col-2 py-5 d-flex rounded my-border-primary mt-4 flex-column div-primary'>";
        echo "<h1 class='text-primary' id='active-cases'>" . $activeToday . "</h1>";
        echo "<h4 class='text-primary'>Total active cases</h4>";
        echo "<h4 class='d-none' id='date'>$threeMonthsAgo - $date</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border-success mt-4 flex-column div-success'>";
        echo "<h1 class='text-success' id='recovered'>" . ($recoveredToday-$recoveredLast3Month) . "</h1>";
        echo "<h4 class='text-success'>Total recovered in the last three months</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border-secondary mt-4 flex-column div-secondary'>";
        echo "<h1 class='text-secondary' id='deaths'>" . ($deathsToday-$deathsLast3Month) . "</h1>";
        echo "<h4 class='text-secondary'>Total deaths in the last three months</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border-danger mt-4 flex-column div-danger'>";
        echo "<h1 class='text-danger' id='confirmed-cases'>" . ($confirmedToday-$confirmedLast3Month) . "</h1>";
        echo "<h4 class='text-danger'>Total confirmed cases in the last three months</h4>";
        echo "</div>";

    }

    


    public function dailyByCountry(){
        self::connect();
        $country = $_GET['country'];
        $today = date("Y-m-d", strtotime('-1 day'));
        $yesterday = date("Y-m-d", strtotime('-2 day'));
        $sql = "SELECT countries.name as country, cases.* FROM cases 
                INNER JOIN countries ON countries.id = cases.country_id
                WHERE `date` = '$today' AND countries.slug = '$country'";
        $sql2 = "SELECT countries.name as country, cases.* FROM cases 
                INNER JOIN countries ON countries.id = cases.country_id
                WHERE `date` = '$yesterday' AND countries.slug = '$country'";

        $stmt = self::$pdo->query($sql);
        $stmt2 = self::$pdo->query($sql2);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $row2 = $stmt2->fetch(PDO::FETCH_ASSOC);


        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-primary'>";
        echo "<h1 class='text-primary' id='active-cases'>" . $row['active'] . "</h1>";
        echo "<h4 class='text-primary'>Total active cases</h4>";
        echo "<h4 class='d-none' id='date'>$today</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-success'>";
        echo "<h1 class='text-success' id='recovered'>" . ($row['recovered']-$row2['recovered']) . "</h1>";
        echo "<h4 class='text-success'>Recovered today</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-secondary'>";
        echo "<h1 class='text-secondary' id='deaths'>" . ($row['deaths']-$row2['deaths']) . "</h1>";
        echo "<h4 class='text-secondary'>Deaths today</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-danger'>";
        echo "<h1 class='text-danger' id='confirmed-cases'>" . ($row['confirmed']-$row2['confirmed']) . "</h1>";
        echo "<h4 class='text-danger'>Confirmed cases today</h4>";
        echo "</div>";

        
        
    }

    public function lastMonth(){
        self::connect();
        $country = $_GET['country'];
        $today = date("Y-m-d", strtotime('-1 day'));
        $monthAgo = date("Y-m-d", strtotime('-1 day -1 month'));
        $sql = "SELECT countries.name as country, cases.* FROM cases 
                INNER JOIN countries ON countries.id = cases.country_id
                WHERE `date` = '$today' AND countries.slug = '$country'";
        $sql2 = "SELECT countries.name as country, cases.* FROM cases 
                INNER JOIN countries ON countries.id = cases.country_id
                WHERE `date` = '$monthAgo' AND countries.slug = '$country'";

        $stmt = self::$pdo->query($sql);
        $stmt2 = self::$pdo->query($sql2);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $row2 = $stmt2->fetch(PDO::FETCH_ASSOC);


        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-primary'>";
        echo "<h1 class='text-primary' id='active-cases'>" . $row['active'] . "</h1>";
        echo "<h4 class='text-primary'>Total active cases</h4>";
        echo "<h4 class='d-none' id='date'>$monthAgo - $today</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-success'>";
        echo "<h1 class='text-success' id='recovered'>" . ($row['recovered']-$row2['recovered']) . "</h1>";
        echo "<h4 class='text-success'>Recovered this month</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-secondary'>";
        echo "<h1 class='text-secondary' id='deaths'>" . ($row['deaths']-$row2['deaths']) . "</h1>";
        echo "<h4 class='text-secondary'>Deaths this month</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-danger'>";
        echo "<h1 class='text-danger' id='confirmed-cases'>" . ($row['confirmed']-$row2['confirmed']) . "</h1>";
        echo "<h4 class='text-danger'>Confirmed cases this month</h4>";
        echo "</div>";
    }

    public function lastThreeMonths(){
        self::connect();
        $country = $_GET['country'];
        $today = date("Y-m-d", strtotime('-1 day'));
        $threeMonthsAgo = date("Y-m-d", strtotime('-1 day -3 month'));
        $sql = "SELECT countries.name as country, cases.* FROM cases 
                INNER JOIN countries ON countries.id = cases.country_id
                WHERE `date` = '$today' AND countries.slug = '$country'";
        $sql2 = "SELECT countries.name as country, cases.* FROM cases 
                INNER JOIN countries ON countries.id = cases.country_id
                WHERE `date` = '$threeMonthsAgo' AND countries.slug = '$country'";

        $stmt = self::$pdo->query($sql);
        $stmt2 = self::$pdo->query($sql2);

        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        $row2 = $stmt2->fetch(PDO::FETCH_ASSOC);

        
        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-primary'>";
        echo "<h1 class='text-primary' id='active-cases'>" . $row['active'] . "</h1>";
        echo "<h4 class='text-primary'>Total active casess</h4>";
        echo "<h4 class='d-none' id='date'>$threeMonthsAgo - $today</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-success'>";
        echo "<h1 class='text-success' id='recovered'>" . ($row['recovered']-$row2['recovered']) . "</h1>";
        echo "<h4 class='text-success'>Recovered in the last three months</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-secondary'>";
        echo "<h1 class='text-secondary' id='deaths'>" . ($row['deaths']-$row2['deaths']) . "</h1>";
        echo "<h4 class='text-secondary'>Deaths in the last three months</h4>";
        echo "</div>";

        echo "<div class='col-2 py-5 d-flex rounded my-border mt-4 flex-column country-bg div-danger'>";
        echo "<h1 class='text-danger' id='confirmed-cases'>" . ($row['confirmed']-$row2['confirmed']) . "</h1>";
        echo "<h4 class='text-danger'>Confirmed cases in the last three months</h4>";
        echo "</div>";
        
    }

    public function allCases(){
        self::connect();
        $sql = "SELECT countries.name as country, cases.* FROM cases INNER JOIN countries ON countries.id = cases.country_id WHERE 1";
        $stmt = self::$pdo->query($sql);

        $cases = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // foreach($cases as $case){
        //     echo $case['country'];
        // }
        return $cases;
    }
    
}